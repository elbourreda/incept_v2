mkdir -p /var/www/html
cp wp-config.php /var/www/html/
cd /var/www/html/
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
mv wp-cli.phar wp
chmod +x wp
./wp core download --allow-root
./wp core install --allow-root --url=$WP_DOMAIN --title=$WP_TILTLE --admin_user=$WP_ADMIN --admin_password=$WP_ADMIN_PASS --admin_email=$WP_EMAIL_ROOT
./wp user create --allow-root $WP_USER $WP_EMAIL_USR --user_pass=$WP_PASS
/usr/sbin/php-fpm7.3 --nodaemonize
